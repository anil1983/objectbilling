# Object Billing using Object Recognition Technology

The main moto of the project is to develop billing software for the commercial stores.
In this we use Object Recognition Technology to identify the object for which we have to bill the amount.

This project will serve the following objectives:-
-------------------------------------------------
maintain list of available products.
maintain description of new products.
Add and maintain new entered category of products.
Provides  reports  weekly or   monthly   and
yearly.
Provides a convenient solution of billing pattern.
An easy way to use environment for users and customers.
